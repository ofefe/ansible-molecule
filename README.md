# Molecule: Installation sur machine innovation

## sur Centos

https://www.rosehosting.com/blog/how-to-install-python-3-6-4-on-centos-7/

ln -s /usr/bin/pip3.6 /usr/bin/pip

## Installation des pré requis

```bash
yum install gcc python3 python3-pip python3-devel openssl-devel libselinux-python -y
```

```bash
pip3 install --upgrade --user setuptools
```

## Installation de molecule

```bash
pip3 install molecule
```

## Installation docker

Ajoute le repo de docker

```bash
yum install yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

Active le repo pour les prérequis

```bash
subscription-manager repos --enable=rhel-7-server-extras-rpms
```

Installation de docker-ce

```bash
yum install docker-ce -y
```

Crée et active le service Docker

```bash
systemctl enable docker.service
systemctl start docker.service
```

Installation de docker-compose

```bash
pip3 install docker-compose
```

personnalisation de docker
[https://docs.docker.com/config/daemon/systemd/#httphttps-proxy](https://docs.docker.com/config/daemon/systemd/#httphttps-proxy)

```bash
mkdir -p /etc/systemd/system/docker.service.d
vi /etc/systemd/system/docker.service.d/http-proxy.conf
```

modifier le fichier http-proxy.conf comme suit:

```bash
[Service]
Environment="HTTP_PROXY=http://158.156.164.40:8080"
# Environment="HTTPS_PROXY=https://158.156.164.40:8080"
Environment="NO_PROXY=localhost,127.0.0.1,*.serv.cdc.fr"
```

relancer le démon docker

```bash
systemctl daemon-reload
systemctl restart docker
```

vérifier la config

```bash
# systemctl show --property=Environment docker
Environment=HTTP_PROXY=http://158.156.164.40:8080 NO_PROXY=localhost,127.0.0.1,*.serv.cdc.fr
```

installer la liaison avec molecule

```bash
pip3 install docker
```

## initialiser un role molecule

```bash
molecule init role -r my-new-role
```

## créer une instance molecule

```bash
cd my-new-role
molecule create
```

yum makecache fast
yum install -y yum-plugin-ovl
yum clean all